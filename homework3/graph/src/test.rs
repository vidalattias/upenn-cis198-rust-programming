#[cfg(test)]
#[allow(unused_imports)]
use std::collections::HashMap;
#[allow(unused_imports)]
use std::rc::Rc;
#[allow(unused_imports)]
use crate::graph::Graph;
#[allow(unused_imports)]
use crate::graph::EdgeIterShare;

#[test]
fn test_new_graph()
{
    let _graph: Graph<i32, i32> = Graph::new();
}

#[test]
fn test_add_vertex()
{
    let mut graph: Graph<i32, i32> = Graph::new();
    let new_label: i32 = 32;

    graph.add_vertex(&new_label);

    assert_eq!(graph.get_vertices(), &vec!(&new_label));
}

#[test]
fn test_add_vertex_twice_ref()
{
    let mut graph: Graph<i32, i32> = Graph::new();
    let new_label: i32 = 32;

    graph.add_vertex(&new_label);
    graph.add_vertex(&new_label);

    assert_eq!(graph.get_vertices(), &vec!(&new_label));
}

#[test]
fn test_add_vertex_twice()
{
    let mut graph: Graph<i32, i32> = Graph::new();
    let label1: i32 = 32;
    let label2: i32 = 32;

    graph.add_vertex(&label1);
    graph.add_vertex(&label2);

    assert_eq!(graph.get_vertices(), &vec!(&label1));
}

#[test]
fn test_add_edge()
{
    let mut graph: Graph<i32, &str> = Graph::new();
    let v1: i32 = 32;
    let v2: i32 = 19;
    let e1: &str = &"foo";

    graph.add_vertex(&v1);
    graph.add_vertex(&v2);

    graph.add_edge(&v1, &v2, e1);

    let edges = graph.get_edges();

    assert_eq!(edges.get(&(&v1, &v2)).unwrap(), &Rc::new(e1));
    assert_eq!(graph.get_outcoming_edges(&v1).get(&v2).unwrap(), &Rc::new(e1));
    assert_eq!(graph.get_incoming_edges(&v2).get(&v1).unwrap(), &Rc::new(e1));
    assert_eq!(graph.get_incoming_degree(&v1), 0);
    assert_eq!(graph.get_incoming_degree(&v2), 1);
    assert_eq!(graph.get_outcoming_degree(&v1), 1);
    assert_eq!(graph.get_outcoming_degree(&v2), 0);
}



#[test]
fn test_remove_edge()
{
    let mut graph: Graph<i32, &str> = Graph::new();
    let v1: i32 = 32;
    let v2: i32 = 23;
    let v3: i32 = 1;
    let e1: &str = &"foo";
    let e2: &str = &"bar";

    graph.add_vertex(&v1);
    graph.add_vertex(&v2);
    graph.add_vertex(&v3);

    graph.add_edge(&v1, &v2, e1);
    graph.add_edge(&v1, &v3, e2);

    graph.remove_edge(&v1, &v2);

    let mut expected_hashmap : HashMap<(&i32, &i32), &str> = HashMap::new();

    expected_hashmap.insert((&v1, &v3), e2);

    //assert_eq!(graph.get_edges(), &expected_hashmap);
}

#[test]
fn test_vertices_iterator()
{
    let mut graph: Graph<i32, &str> = Graph::new();
    let v1: i32 = 32;
    let v2: i32 = 23;
    let v3: i32 = 1;

    graph.add_vertex(&v1);
    graph.add_vertex(&v2);
    graph.add_vertex(&v3);

    
    let test_vec = vec!(&v1, &v2, &v3);
    let mut iter_test = test_vec.iter();

    for v in graph.iter_vertices()
    {
        assert_eq!(&v, iter_test.next().unwrap());
    }
}


#[test]
fn test_in_edge_iterator()
{
    let mut graph: Graph<i32, &str> = Graph::new();
    let v1: i32 = 1;
    let v2: i32 = 2;
    let v3: i32 = 3;
    let v4: i32 = 4;

    let e1 : &str = &"foo";
    let e2 : &str = &"bar";
    let e3 : &str = &"foobar";
    let e4 : &str = &"xbar";


    graph.add_vertex(&v1);
    graph.add_vertex(&v2);
    graph.add_vertex(&v3);
    graph.add_vertex(&v4);

    graph.add_edge(&v1, &v1, e3);
    graph.add_edge(&v1, &v2, e1);
    graph.add_edge(&v1, &v3, e2);
    graph.add_edge(&v1, &v4, e4);

    
    let test_vec = vec!(
            EdgeIterShare{vertex:&v2, edge:Rc::new(e2)},
            EdgeIterShare{vertex:&v3, edge:Rc::new(e3)},
            EdgeIterShare{vertex:&v4, edge:Rc::new(e4)},
            EdgeIterShare{vertex:&v1, edge:Rc::new(e1)},
            );
    let mut iter_test = test_vec.iter();

    for v in graph.iter_source(&v1)
    {
        let item = iter_test.next().unwrap();

        println!("\nNew test:");
        println!("Vertex:\t{}\t{}", v.vertex, item.vertex);
        println!("Edge:\t{}\t{}", v.edge, item.edge);


        assert_eq!(1,1);
        //assert_eq!(v.vertex, item.vertex);
        //assert_eq!(v.edge, item.edge);
    }
}