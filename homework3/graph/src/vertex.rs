pub struct Vertex<V>
{
    label: V,
}

impl<V> Vertex<V>
{
    pub fn new(label: V) -> Vertex<V>
    {
        Vertex
        {
            label
        }
    }
}