use std::collections::HashMap;
use std::hash::Hash;
use std::fmt;
use std::rc::Rc;

#[derive(Debug)]
pub struct EdgeIterShare<'a, V, E>
{
    pub vertex: &'a V,
    pub edge: Rc<E>,
}

#[derive(Clone, Debug, Default)]
pub struct Graph<'a,V,E> where
{
    vertices: Vec<&'a V>,
    edges: HashMap<(&'a V, &'a V), Rc<E>>,
    vertex_id_counter: usize,
    id_to_vert: HashMap<usize, V>,
    vert_to_id: HashMap<&'a V, usize>,
    outcoming_edges: HashMap<&'a V, HashMap<&'a V, Rc<E>>>,
    incoming_edges: HashMap<&'a V, HashMap<&'a V, Rc<E>>>,
}

impl<'a,V,E> Graph<'a,V,E> where
V : Eq,
V : Hash,
V: fmt::Debug,
{
    pub fn new() -> Graph<'a,V,E>
    {
        Graph{
            vertices: Vec::new(),
            edges: HashMap::new(),
            vertex_id_counter: 0,
            id_to_vert: HashMap::new(),
            vert_to_id: HashMap::new(),
            outcoming_edges: HashMap::new(),
            incoming_edges: HashMap::new(),
        }
    }

    pub fn add_vertex(&mut self, label: &'a V)
    {
        if !self.vertices.iter().any(|x| *x==label){
            self.outcoming_edges.insert(label, HashMap::new());
            self.incoming_edges.insert(label, HashMap::new());

            self.vertices.push(label);
        }
    }

    pub fn add_edge(&mut self, v1: &'a V, v2: &'a V, label: E)
    {
        if self.vertices.iter().any(|x| *x==v2)
        {
            if self.vertices.iter().any(|x| *x==v1)
            {
                let rc_label = Rc::new(label);
                self.edges.insert((v1,v2), Rc::clone(&rc_label));

                self.outcoming_edges.get_mut(v1).unwrap().insert(v2, Rc::clone(&rc_label));
                self.incoming_edges.get_mut(v2).unwrap().insert(v1, Rc::clone(&rc_label));
            }
            else{
                panic!("Vertices not present in graph (2).");
            }
        }
        else{
            panic!("Vertices not present in graph (1).");
        }
    }

    pub fn exists_vertex(&mut self, label: &'a V)
    {
        if !self.vertices.iter().any(|x| *x==label)
        {
            self.add_vertex(label);
        }
    }

    pub fn exists_edge(&mut self, v1: &'a V, v2: &'a V, label: E)
    {
        if !self.edges.keys().any(|x| x==&(v1, v2))
        {
            self.edges.insert((v1,v2), Rc::new(label));
        }
    }

    pub fn remove_edge(&mut self, v1: &'a V, v2: &'a V)
    {
        self.edges.remove(&(v1,v2));
    }


    pub fn get_vertices(&'a self) -> &'a Vec<&'a V>
    {
        &self.vertices
    }

    pub fn get_edges(&'a self) -> &'a HashMap<(&'a V, &'a V), Rc<E>>
    {
        &self.edges
    }

    pub fn get_incoming_edges(&'a self, vertex: &'a V) -> &'a HashMap<&'a V, Rc<E>>
    {
        &self.incoming_edges.get(vertex).unwrap()
    }

    pub fn get_outcoming_edges(&'a self, vertex: &'a V) -> &'a HashMap<&'a V, Rc<E>>
    {
        &self.outcoming_edges.get(vertex).unwrap()
    }

    pub fn get_incoming_degree(&'a self, vertex: &'a V) -> usize
    {
        self.incoming_edges.get(vertex).unwrap().len()
    }

    pub fn get_outcoming_degree(&'a self, vertex: &'a V) -> usize
    {
        self.outcoming_edges.get(vertex).unwrap().len()
    }

    pub fn iter_vertices(&self) -> impl Iterator<Item = &V> + '_
    {
        struct VertexIterator<'a, V>
        {
            vertices_it: &'a Vec<&'a V>,
            index: usize,
        }

        impl<'a, V> Iterator for VertexIterator<'a, V> {
            // we will be counting with usize
            type Item = &'a V;
        
            // next() is the only required method
            fn next(&mut self) -> Option<Self::Item> {
                // Increment our count. This is why we started at zero.
                if self.index == self.vertices_it.len()
                {
                    return None;
                }
                let ret_val = Some(self.vertices_it[self.index]);
                self.index+=1;
                ret_val
            }
        }

        VertexIterator{
            vertices_it: &self.vertices,
            index: 0
        }
    }

    pub fn iter_source(&self, v: &'a V) -> impl Iterator<Item = EdgeIterShare<'_, V, E>> + '_
    {
        struct EdgeIterator<'a, V, E>
        {
            hashmap: &'a HashMap<&'a V, Rc<E>>,
            keys: std::collections::hash_map::Keys<'a, &'a V, Rc<E>>,
        }

        impl<'a, V, E> Iterator for EdgeIterator<'a, V, E> 
        where
            V : Eq,
            V : Hash,
        {
            type Item = EdgeIterShare<'a, V, E>;
            fn next(&mut self) -> Option<Self::Item> {
                // Increment our count. This is why we started at zero.
                let current_index: Option<&&'a V> = self.keys.next();

                match current_index
                {
                    None => None,
                    Some(x) => Some(
                        EdgeIterShare{
                            vertex: x,
                            edge:(*self.hashmap).get(x).unwrap().clone()
                        }
                    )
                }
            }
        }

        EdgeIterator{
            hashmap: &self.outcoming_edges.get(v).unwrap(),
            keys: self.outcoming_edges.get(v).unwrap().keys(),
        }
    }


    pub fn iter_target(&self, v: &'a V) -> impl Iterator<Item = EdgeIterShare<'_, V, E>> + '_
    {
        struct EdgeIterator<'a, V, E>
        {
            hashmap: &'a HashMap<&'a V, Rc<E>>,
            keys: std::collections::hash_map::Keys<'a, &'a V, Rc<E>>,
        }

        impl<'a, V, E> Iterator for EdgeIterator<'a, V, E> 
        where
            V : Eq,
            V : Hash,
        {
            type Item = EdgeIterShare<'a, V, E>;
            fn next(&mut self) -> Option<Self::Item> {
                // Increment our count. This is why we started at zero.
                let current_index: Option<&&'a V> = self.keys.next();

                match current_index
                {
                    None => None,
                    Some(x) => Some(
                        EdgeIterShare{
                            vertex: x,
                            edge:(*self.hashmap).get(x).unwrap().clone()
                        }
                    )
                }
            }
        }

        EdgeIterator{
            hashmap: &self.incoming_edges.get(v).unwrap(),
            keys: self.incoming_edges.get(v).unwrap().keys(),
        }
    }


    pub fn merge_vertices(&self, v1: &'a V, v2:  &'a V)
    {
        for vertex in self.incoming_edges.get(v2)
        {
            for vertex_bis in vertex.keys()
            {
                
            }
        }
    }
}