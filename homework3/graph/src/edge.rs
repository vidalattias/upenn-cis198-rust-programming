pub struct Edge<E>
{
    label: E,
}

impl<E> Edge<E>
{
    pub fn new(label: E) -> Edge<E>
    {
        Edge{
            label,
        }
    }
}