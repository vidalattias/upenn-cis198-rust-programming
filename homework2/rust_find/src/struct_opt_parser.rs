use structopt::StructOpt;
use std::path::PathBuf;
use regex::Regex;

#[derive(Debug, StructOpt)]
#[structopt(name = "Rust Find", about = "A simple CLI Find command in Rust.")]
pub struct CLI {
    #[structopt(short, long)]
    pub patterns: Vec<String>,
    #[structopt(short, long)]
    pub dirs: Vec<String>,
    #[structopt(short, long)]
    pub output: String,
    #[structopt(long, default_value = "3")]
    pub depth: i32,
}

#[derive(Debug)]
pub struct Args{
    pub patterns : Vec<Regex>,
    pub dirs : Vec<PathBuf>,
    pub output : String,
    pub depth : i32,
}

impl Args{
    pub fn new() -> Args
    {
        let args = CLI::from_args();
        Args{
            patterns : args.patterns.into_iter().map(|x| Regex::new(&x).unwrap()).collect(),
            dirs : args.dirs.into_iter().map(|x| PathBuf::from(x)).collect(),
            output : args.output,
            depth : args.depth,
        }
    }
}
