use std::path::PathBuf;
use regex::Regex;

use crate::file_representation;

pub fn crawl_dir(path: PathBuf, regexes : Vec<Regex>, depth: i32) -> Vec<file_representation::FileRepresentation>{
    let mut ret_vec : Vec<file_representation::FileRepresentation> = Vec::new();

    let mut dirs = path.read_dir();
    for entry in dirs {
        let entry = match entry {
            Ok(entry) => entry,
            Err(e) => panic!("Failed to get dirs: {}", e),
        };

        if entry.path().is_dir() && depth > 0{
            ret_vec.append(&mut crawl_dir(entry.path(), regexes.clone(), depth-1));
        }
        else{
            if entry.path().is_file()
            {
                for regex in &regexes{
                    if regex.is_match(entry.path().to_str().unwrap()){
                        ret_vec.push(file_representation::FileRepresentation::new(&entry.path()));
                        break;
                    }
                }
            }
        }
    }
    ret_vec
}
