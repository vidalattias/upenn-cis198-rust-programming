use std::path::PathBuf;

enum FileType
{
    Audio,
    Video,
    Code,
    Unknown,
}

impl FileType
{
    fn to_string(&self) -> String {
        match self{
            FileType::Audio => "Audio".to_string(),
            FileType::Video => "Video".to_string(),
            FileType::Code => "Code".to_string(),
            FileType::Unknown => "Unknown".to_string(),
        }
    }
}

pub struct FileRepresentation
{
    parents : Vec<String>,
    name : String,
    kind : FileType,
}

impl FileRepresentation
{
    pub fn new(path : &PathBuf) -> FileRepresentation
    {
        let mut parents : Vec<String> = Vec::new();
        for i in path.iter()
        {
            parents.push(i.to_str().unwrap().to_string());
        }
        parents.truncate(parents.len()-1);

        let kind = match path.extension()
        {
            None => FileType::Unknown,
            Some(x) => match x.to_str().unwrap(){
                "mp3" => FileType::Audio,
                "flac" => FileType::Audio,
                "wav" => FileType::Audio,
                "mp4" => FileType::Video,
                "avi" => FileType::Video,
                "mkv" => FileType::Video,
                "rs" => FileType::Code,
                "php" => FileType::Code,
                "py" => FileType::Code,
                "cpp" => FileType::Code,
                "hpp" => FileType::Code,
                "c" => FileType::Code,
                "h" => FileType::Code,
                _ => FileType::Unknown,
            },
        };

        FileRepresentation{
            parents : parents,
            name : path.file_name().unwrap().to_str().unwrap().to_string(),
            kind : kind,
        }
    }

    pub fn to_string(&self) -> String
    {
        let mut ret_string : String = self.kind.to_string()  + "\t";
        for parent in &self.parents
        {
            ret_string += &(parent.to_owned() + "/");
        }

        ret_string += &self.name.to_owned();

        ret_string.to_owned()
    }
}
