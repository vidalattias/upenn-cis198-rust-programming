pub mod struct_opt_parser;
pub mod crawler;
pub mod file_representation;

fn main() {
    let args = struct_opt_parser::Args::new();

    let mut result_vec : Vec<file_representation::FileRepresentation> = Vec::new();

    for path in args.dirs.iter(){
        result_vec.append(&mut crawler::crawl_dir(path.to_path_buf(), args.patterns.clone(), args.depth));
    }

    for file in &result_vec
    {
        println!("{}", file.to_string());
    }
}
