/*
    CIS198 Homework 1
    Part 1: Implementing functions

    Complete and write at least one unit test for each function you implement.
    If it already has a unit test, either add assertions to it or add a new one.
    Also answer the questions in text.
*/


/*
    Problem 1: Double

    Implement the function that doubles an integer in three different ways.

    What are some differences between them? Can you write unit tests
    which fail (or fail to compile) for some but not others?

    Which of the three do you prefer?
*/


/*

The differences between the three functions :

double_v1(n: i32) -> i32 takes an i32 as part of its stack and returns a new i32, everything is completely independant from the calling code.

double_v2(n: &i32) -> i32 takes a non mutable reference to an i32 belonging to the calling code's stack and returns a new i32.

double_v3(n: &mut i32) takes a mutable reference to an i32 belonging to the calling's stack and modifies it directly and then releases the ownership without returning anything.

The first and third are equally preferable, it is only a matter of the usecase, the first one is interesting if the doubled value should not interfere with the original one whereas the third one is interesting in the case we want to double directly the original variable, then there is no need to write in the return pointer nor to manage the returned value.
*/


pub fn double_v1(n: i32) -> i32 {
    2*n
}

pub fn double_v2(n: &i32) -> i32 {
    2*n
}

pub fn double_v3(n: &mut i32) {
    // double n in place
    *n *= 2;
}

// Example unit test (so you can recall the syntax)
#[test]
fn test_double_v1() {
    assert_eq!(double_v1(2), 4);
    assert_eq!(double_v1(-3), -6);
}

#[test]
fn test_double_v2() {
    assert_eq!(double_v2(&2), 4);
    assert_eq!(double_v2(&-3), -6);
}

#[test]
fn test_double_v3() {
    let mut n = 4;
    double_v3(&mut n);
    assert_eq!(n, 8);
}


/*
    Problem 2: Integer square root

    Implement the integer square root function: sqrt(n) should return the
    largest m such that m * m <= n. For a 'harder' version, try to do it more
    efficiently than trying every possibility.
*/

pub fn sqrt(n: usize) -> usize {
    for j in 1..n
    {
        if j*j > n
        {
            return j-1;
        }
    }
    0
}


// Using the babylonian algorithm that uses only integers found here in Stack Exchange https://math.stackexchange.com/questions/2469446/fast-algorithm-for-computing-integer-square-roots-on-machines-that-doesnt-suppo
pub fn sqrt_harder(n: usize) -> usize
{
    // We need to test this case otherwise there would be an issue with overflowing afterward considering that 18446744073709551615 is the max of usize on a 64 bits architecture. We did not take into consideration other architectures but we should have.
    if n == 18446744073709551615
    {
        return 4294967295;
    }
    let mut x : usize = n;
    let mut y : usize = 1;

    while x > y
    {
        x = (x+y)/2;
        y = n/x;
    }
    x
}

#[test]
pub fn test_sqrt() {
    assert_eq!(sqrt(25), 5);
}


#[test]
pub fn test_sqrt_perfect_square() {
    assert_eq!(sqrt(25), 5);
}

#[test]
pub fn test_sqrt_general() {
    assert_eq!(sqrt(30), 5);
}

#[test]
pub fn test_sqrt_harder_perfect_square() {
    assert_eq!(sqrt_harder(25), 5);
}


#[test]
pub fn test_sqrt_harder_general() {
    assert_eq!(sqrt_harder(30), 5);
}


/*If we were to run this test, it would take a very long time.
#[test]
pub fn test_sqrt_long() {
    assert_eq!(sqrt(18446744073709551615), 4294967295);
    assert_eq!(sqrt(18446744073709551614), 4294967295);
}
*/

#[test]
pub fn test_sqrt_harder_long() {
    assert_eq!(sqrt_harder(18446744073709551615), 4294967295);
    assert_eq!(sqrt_harder(18446744073709551614), 4294967295);
}


// Remember to write unit tests here (and on all future functions)

/*
    Problem 3: Slice sum

    Implement the sum function on slices in two different ways
    (using different for loop patterns).
    Do not use the predefined sum function.
    Also, try to do it without an unnecessary `return` statement at the end --
    Clippy should detect if you mess this up.

    Which of the two ways do you prefer?
*/

/*

I prefer the sum_v1 way because it is less verbose however for pedagogical purpose, the sum_v2 is clearer about what happens under the hood. I was not sure about the fact of not using the return statement, I hope this is what was expected.

*/

pub fn sum_v1(slice: &[i32]) -> i32 {
    let mut sum = 0;
    for &v in slice {
        sum += v;
    }
    sum
}

pub fn sum_v2(slice: &[i32]) -> i32 {
    let mut sum = 0;
    for v in slice.iter() {
        sum += v;
    }
    sum
}


#[test]
pub fn test_sum_v1() {
    let x : [i32; 4] = [1,2,3,4];
    assert_eq!(sum_v1(&x), 10);
}

#[test]
pub fn test_sum_v2() {
    let x : [i32; 4] = [1,2,3,4];
    assert_eq!(sum_v2(&x), 10);
}



/*
    Problem 4: Unique

    Make unique. Create a new vector which contains each item in the vector
    only once! Much like a set would.
    This doesn't need to be efficient; you can use a for loop.
*/

pub fn unique(slice: &[i32]) -> Vec<i32> {
    let mut return_vec : Vec<i32> = Vec::new();
    for &v in slice
    {
        let mut is_new = true;
        for v2 in &return_vec{
            if v == *v2
            {
                is_new = false;
            }
        }

        if is_new
        {
            return_vec.push(v);
        }
    }
    return_vec
}


// We assume in this test that the order of unique elements is the same order as the first representative of each element in the original array.
#[test]
pub fn test_unique() {

    let x : [i32; 10] = [2,7,3,5,2,3,6,5,6,3];

    let y_verif : Vec<i32> = vec![2, 7, 3, 5, 6];
    let y : Vec<i32> = unique(&x);
    assert_eq!(y, y_verif);
}

/*
    Problem 5: Filter

    Return a new vector containing only elements that satisfy `pred`.
    This uses some unfamiliar syntax for the type of pred -- all you need
    to know is that pred is a function from i32 to bool.
*/
pub fn filter(slice: &[i32], pred: impl Fn(i32) -> bool) -> Vec<i32> {
    let mut return_vec : Vec<i32> = Vec::new();

    for &v in slice {
        if pred(v)
        {
            return_vec.push(v);
        }
    }
    return_vec
}

#[test]
fn test_filter() {
    fn is_even(n: i32) -> bool {
        n % 2 == 0
    }
    assert_eq!(filter(&vec![1, 2, 3, 4, 5, 6], &is_even), vec![2, 4, 6]);
}

/*
    Problem 6: Fibonacci

    Given starting fibonacci numbers n1 and n2, compute a vector of
    length 'out_size'
    where v[i] is the ith fibonacci number.
*/
pub fn fibonacci(n1: i32, n2: i32, out_size: usize) -> Vec<i32> {
    if out_size == 0
    {
        return Vec::new();
    }
    if out_size == 1
    {
        return vec![n1];
    }

    let mut return_vec : Vec<i32> = vec![n1, n2];
    for i in 2..out_size
    {
        return_vec.push(return_vec[i-2] + return_vec[i-1]);
    }
    return_vec
}

#[test]
pub fn test_fibonacci_0() {
    assert_eq!(fibonacci(1, 1, 0), vec![]);
}

#[test]
pub fn test_fibonacci_1() {
    assert_eq!(fibonacci(4, 1, 1), vec![4]);
}

#[test]
pub fn test_fibonacci_10() {
    assert_eq!(fibonacci(1, 1, 10), vec![1, 1, 2, 3, 5, 8, 13, 21, 34, 55]);
}

/*
    Problem 7: String concatenation

    Create a function which concats 2 &strs and returns a String,
    and a function which concats 2 Strings and returns a String.

    You may use any standard library function you wish.

    What are some reasons the second function is not efficient?
*/


/*
    The second function is not efficient because we have to reallocate the two String in order to prevent borrowing.
*/

pub fn str_concat(s1: &str, s2: &str) -> String {
    s1.to_owned() + s2
}

pub fn string_concat(mut s1: String, s2: String) -> String {
    s1.push_str(&s2);
    s1
}



#[test]

pub fn test_str_concat() {
    let s1 : &str = "hi";
    let s2 : &str = " there";
    let s3 : String = str_concat(s1, s2);

    assert_eq!(s1, "hi");
    assert_eq!(s2, " there");
    assert_eq!(s3, "hi there");
}

pub fn test_string_concat() {
    let s1 : String = "hi".to_owned();
    let s2 : String = " there".to_owned();
    let s3 : String = string_concat(s1.to_owned(), s2.to_owned());

    assert_eq!(s1, "hi");
    assert_eq!(s2, " there");
    assert_eq!(s3, "hi there");
}



/*
    Problem 8: String concatenation continued

    Convert a Vec<String> into a String.
    Your answer to the previous part may help.
*/

pub fn concat_all(v: Vec<String>) -> String {
    if v.is_empty() {
        return "".to_owned();
    }

    let mut return_string : String = "".to_owned();

    for elt in v {
        return_string = string_concat(return_string.to_owned(), elt.to_owned());
    }
    return_string
}

#[test]
pub fn test_concat_all_empty () {
    let vector : Vec<String> = vec![];

    let received_string : String = concat_all(vector);

    assert_eq!(received_string, "".to_owned());
}

#[test]
pub fn test_concat_all_not_empty () {
    let vector : Vec<String> = vec!["Hello".to_owned(), " my".to_owned()," name".to_owned()," is".to_owned(), " Vidal!".to_owned()];


    let received_string : String = concat_all(vector);

    assert_eq!(received_string, "Hello my name is Vidal!".to_owned());
}

/*
    Problem 9: Parsing

    Convert a Vec<String> into a Vec<i32> and vice versa.

    Assume all strings are correct numbers! We will do error handling later.
    Use `.expect("ignoring error")` to ignore Result from parse()
    See https://doc.rust-lang.org/std/primitive.str.html#method.parse

    The unit tests check if your functions are inverses of each other.

    A useful macro: format! is like println! but returns a String.
*/

pub fn parse_all(v: Vec<String>) -> Vec<i32> {
    let mut vector : Vec<i32> = Vec::new();

    for elt in v
    {
        vector.push(elt.parse().expect("ignoring error"));
    }

    vector
}

pub fn print_all(v: Vec<i32>) -> Vec<String> {
    let mut vector : Vec<String> = Vec::new();

    for elt in v
    {
        vector.push(elt.to_string());
    }

    vector
}

#[test]
fn test_print_parse() {
    assert_eq!(parse_all(print_all(vec![1, 2])), vec![1, 2]);
}

#[test]
fn test_parse_print() {
    let v = vec!["1".to_string(), "2".to_string()];
    assert_eq!(print_all(parse_all(v.clone())), v);
}

/*
    Problem 10: Composing functions

    Implement a function which concatenates the even Fibonacci
    numbers out of the first n Fibonacci numbers.

    For example: if n = 6, the first 5 Fibonacci numbers are 1, 1, 2, 3, 5, 8,
    so the function should return the String "28".

    Don't use a for loop! Your previous functions should be sufficient.
*/

pub fn concat_even_fibonaccis(n: usize) -> String {

    fn is_even(n: i32) -> bool {
        n % 2 == 0
    }

    let vector : Vec<i32> = filter(&fibonacci(1, 1, n), is_even);

    concat_all(print_all(vector))
}

#[test]
fn test_concat_even_fibonaccis() {
    assert_eq!(&concat_even_fibonaccis(6), "28");
    assert_eq!(&concat_even_fibonaccis(9), "2834");
}
