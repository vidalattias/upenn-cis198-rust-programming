/*
    CIS198 Homework 1
    Part 2: Strings, files, and mutability

    Make the following failing functions/tests pass.
    Answer the questions as a comment next to the problems.
*/


use std::fs::File;
use std::io::Read;

/*
    Problem 1: Split variants

    Create functions split_ref and split_clone such that
    all the following tests will pass. Feel free to use Rust's split method
    (https://doc.rust-lang.org/std/primitive.slice.html#method.split)
    as needed.
*/

// split_ref must have the return type Vec<&str>
// split_clone must have the return type Vec<String>

pub fn split_ref(s : &str) -> Vec<&str>{
    let split = s.split(' ');
    split.collect()
}

#[test]
fn test_split_ref(){
    let string = "Hello World!".to_string();
    assert_eq!(split_ref(& string), ["Hello", "World!"]);
    assert_eq!(split_ref("Hello World!"), & ["Hello", "World!"]);
    assert_eq!(split_ref("Hello World!"), vec!["Hello", "World!"]);
}


pub fn split_clone(s: &str) -> Vec<String>{
    let split = s.split(' ');
    let mut return_vec : Vec::<String> = Vec::new();

    for elt in split{
        return_vec.push(elt.to_string());
    }

    return_vec
}

#[test]
fn test_split_clone(){
     let string = "Hello World!".to_string();
    assert_eq!(split_clone(& string), ["Hello", "World!"]);
     assert_eq!(split_clone("Hello World!"), & ["Hello", "World!"]);
     assert_eq!(split_clone("Hello World!"), vec!["Hello", "World!"]);
 }

/*
    Problem 2: Longest string

    Write function pick_longest which picks the longests of two &str arguments.
    Taking &str arguments makes it more general than taking Strings.
    Return a new String (we will see later how to return a &str.)
*/


pub fn pick_longest(s1 : &str, s2 : &str) -> String
{
    if s1.len() >= s2.len(){
        s1.to_string()
    }
    else{
        s2.to_string()
    }
}

#[test]
fn test_pick_longest() {
    assert_eq!(
        pick_longest(& "cat".to_string(), & "dog".to_string()),
        "cat".to_string()
    );
}

// Question 1:
// For the curious, attempt to return reference, that is:
//
// fn pick_longest(s1: &str, s2: &str) -> &str
//
// What goes wrong when you try to implement this function? Why is this
// the case?

/*

When we try to implement this function in the way proposed above we get an error saying the lifetime is not specified and suggests to precise all variables lifetimes. This is due to the fact that there are two input references here and the second rule of elision cannot infer what type to give to the output hence the need to annotate explicitly the lifetimes.

pub fn pick_longest_ref<'a>(s1 : &'a str, s2 : &'a str) -> &'a str
{
    if s1.len() >= s2.len(){
        s1
    }
    else{
        s2
    }
}

*/



/*
    Problem 3: File to string

    Write a function that returns all the contents of a file as a single String.

    DO NOT USE the assocated function std::fs::read_to_string

    Instead use File::open, and the method read_to_string
    (https://doc.rust-lang.org/std/io/trait.Read.html#method.read_to_string)

    You can use .expect("ignoring error: ") to ignore the Result<...> type in open()
    and read_to_string. We will discuss error handling later.
*/

pub fn file_to_string(path: &str) -> String {
    let mut file = File::open(path).expect("ignoring error: ");
    let mut ret_str : String = String::new();
    file.read_to_string(&mut ret_str).expect("ignoring error: ");
    ret_str
}

#[test]
fn test_file_to_string() {
    assert_eq!(
        file_to_string("file.txt"), "hi my name
is Vidal
Here is some unicode שלום ٱلسَّلَامُ عَلَيْكُمْ
😁 👈👕👉 👖
".to_string()
    );
}

/*
    Problem 4: Mutability

    Why does the following implementation not work as expected?
    Fix by changing the type signature of add1 and the way it's called on add1_test().
    do NOT change the return type.
*/



#[test]
fn test_add1() {
    let mut x = 1;
    add1(&mut x);
    assert_eq!(x, 2);
}

pub fn add1(x: &mut i32) {
    (*x) += 1;
}

// This code did not work because the value of x was direclty copied because it totally fits in the stack. We need to use a mutable reference to x in order to keep track of its changes.

/*
    Problem 5: Mutability continued

    The error says: cannot assign to immutable borrowed content `*str1`
    But we declared it mutable? Fix by changing only the line below.
*/
pub fn mut2() {
    let hello = String::from("hello");
   // CHANGE ONLY THIS LINE:
   let str1: &mut String = &mut String::from("str1");
   *str1 = hello;
}
