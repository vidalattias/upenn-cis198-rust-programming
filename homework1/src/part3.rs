/*
    CIS198 Homework 1
    Part 3: Ownership, move semantics, and lifetimes

    Complete and write at least one unit test for each function you implement.
    If it already has a unit test, either add assertions to it or add a new one.
    Also answer the questions in text.
*/



/*
    Problem 1: Swap ints

    Implement the function that swaps two integers, and write unit tests.

    The Rust borrow checker may help avoid some possible bugs.

    Then answer this question:
    Q: A common source of error in swap implementations is failing to work if
       the two references are the same. Why don't you need to worry about this
       case in Rust?

    (Try writing a unit test where they are both
    the same, i.e. swap_ints(&mut x, &mut x).)
*/
pub fn swap_ints(x1: &mut i32, x2: &mut i32) {
    std::mem::swap(&mut (*x1), &mut (*x2))
}

#[test]
pub fn test_swap_ints_different()
{
    let mut x : i32 = 5;
    let mut y : i32 = 8;

    swap_ints(&mut x, &mut y);
    assert_eq!(x, 8);
    assert_eq!(y, 5);
}


/*
#[test]
pub fn test_swap_ints_same()
{
    let mut x : i32 = 5;

    mem::swap(&mut x, &mut x);
    assert_eq!(x, 5);
}

This configuration is not possible because there would be a double borrow.

*/



/*
    Problem 2: String duplication
*/


#[test]
fn copy_string_test() {
    let str1 = String::from("foo");
    let str2 = str1.clone();
    assert_eq!(str1, str2);
}


// This test doesn't work. Fix it by copying strings properly.
// Q1. What went wrong?

// What went wront is that str1 memory was borrowed by str2 and then str1 was not safe anymore to access. This is mainly due to the fact that the actual memory of String is stored in the heap and needs to be cloned.

// Q2. How come it works fine here?

// Here it works fine because we are dealing with integers which are stored in the stack. Then when copying we directly clone them which is not true for Strings.
#[test]
fn copy_int_test() {
    let i1 = 1;
    let i2 = i1;
    assert_eq!(i1, i2);
}

// Now implement the following function that duplicates a string n times.
pub fn duplicate_string(s: &str, times: usize) -> Vec<String> {
    let mut ret_vec : Vec::<String> = Vec::new();
    for _ in 0..times
    {
        ret_vec.push(s.to_string());
    }

    ret_vec
}

#[test]
fn test_copy_string_duplicate() {
    let duplicated_string : Vec<String> = duplicate_string("test", 10);

    for i in 0..10
    {
        assert_eq!(duplicated_string[i], "test");
        //assert_eq!(duplicate_string[i], "test");
    }

    assert_eq!(duplicated_string.len(), 10);
}



/*
    Problem 3: String duplication continued

    These two don't work either. Fix by changing the type of "string" in the
    function copy_me ONLY, and by adjusting the parameter to "copy_me" where
    it's called.
*/

pub fn copy_me(string: &str) -> String {
    string.to_string()
}

#[test]
fn copy_me_test() {
    let str1 = String::from("foo");
    assert_eq!(str1, copy_me(&str1));
}

#[test]
fn copy_me_test2() {
    let str1 = String::from("foo");
    let str2 = copy_me(&str1);
    assert_eq!(str1, str2);
}

/*
    Problem 4: Lifetime specifiers

    For each of the following three functions, either implement it by adding lifetime specifiers, or explain why this is not possible.

    (It's not truly impossible -- we will see later on that advanced features
    such as "unsafe code" can be used to turn off Rust's safety and lifetime
    checks.)
*/
// fn new_ref_string() -> &String {
//     let new_string = String::from("foo");
//     &new_string
// }

// The first one is impossible to implement because the String is created inside the function then the lifetime is shorter than the function.


// fn new_ref_str() -> &str {
//     unimplemented!();
// }

pub fn new_ref_str<'a>() -> &'a str {
    let string = "foo";
    &string
}

#[test]

fn test_new_ref_str()
{
    assert_eq!(new_ref_str(), "foo");
}


// The same function from part2
// fn pick_longest2(s1: &str, s2: &str) -> &str {
//     unimplemented!()
// }

pub fn pick_longest2<'a>(s1: &'a str, s2: &'a str) -> &'a str {
    if s1.len() >= s2.len(){
        s1
    }
    else{
        s2
    }
}


/*
    Problem 5: Using functions with lifetimes

    Write two versions of a function which returns the longest string in a
    vector, using pick_longest2 as a helper function.

    If the vector is empty, return "".

    Q1. In pick_longest_in_v2, if you were to explicitly specify the lifetime
        of the input and output, what should it be?

    Q2. What are the pros and cons of v1 and v2?
*/

pub fn pick_longest_in_v1(v: Vec<String>) -> String {
    if v.is_empty(){
        "".to_string()
    }

    else{
        let mut longest : &str = &v[0];

        for elt in &v
        {
            longest = pick_longest2(longest, elt);
        }

        longest.to_string()
    }
}

pub fn pick_longest_in_v2(v: Vec<&str>) -> &str {
    if v.is_empty(){
        &""
    }

    else{
        let mut longest : &str = &v[0];

        for elt in &v
        {
            longest = pick_longest2(longest, elt);
        }

        longest
    }
}


#[test]
pub fn pick_longest_in_v1_test() {
    let vector : Vec<String> = Vec::new();
    assert_eq!(pick_longest_in_v1(vector), "");
}


#[test]
pub fn pick_longest_in_v1_empty_test() {
    let vector : Vec<String> = vec!["foo".to_string(), "bar".to_string(), "foobar".to_string()];
    assert_eq!(pick_longest_in_v1(vector), "foobar");
}


#[test]
pub fn pick_longest_in_v2_test() {
    let vector : Vec<&str> = Vec::new();
    assert_eq!(pick_longest_in_v2(vector), "");
}


#[test]
pub fn pick_longest_in_v2_empty_test() {
    let vector : Vec<&str> = vec![&"foo", &"bar", &"foobar"];
    assert_eq!(pick_longest_in_v2(vector), "foobar");
}


// Q1 answer : I would put the following lifetimes :  fn pick_longest_in_v2(v: Vec<&'static str>) -> &'static str because str have an unlimited lifetime.

// Q2 answer :
// pros v1 : it has the classical pros of String over str : it is dynamic and more suitable for strings that change over time and is shared as a reference to a memory in the heap. Cons : a lot of implicit conversions during the evaluation

// pros v2 : we work directly with str so there is no convertion made.


/*
    Problem 6: Move semantics

    Write three versions of a function that pads a vector with zeros.
    Fail if the vector is larger than the desired length.

    Use .clone() if necessary to make any additional unit tests compile.

    Which of these functions do you prefer? Which is the most efficient?
*/

pub fn pad_with_zeros_v1(v: Vec<usize>, desired_len: usize) -> Vec<usize> {
    let mut result : Vec<usize> = Vec::new();
    let filled : usize = v.len();

    assert!((filled <= desired_len), "Vector is larger than the desired length!");

    for elt in v
    {
        result.push(elt);
    }

    result.resize(desired_len, 0);


    debug_assert_eq!(result.len(), desired_len);

    result
}

pub fn pad_with_zeros_v2(slice: &[usize], desired_len: usize) -> Vec<usize> {
    let mut result : Vec<usize> = Vec::new();
    let filled : usize = slice.len();

    assert!((filled <= desired_len), "Vector is larger than the desired length!");

    for elt in slice
    {
        result.push(*elt);
    }

    result.resize(desired_len, 0);


    debug_assert_eq!(result.len(), desired_len);

    result
}

pub fn pad_with_zeros_v3(v: &mut Vec<usize>, desired_len: usize) {
    let filled : usize = v.len();

    assert!((filled <= desired_len), "Vector is larger than the desired length!");

    for _ in 0..(desired_len-filled)
    {
        v.push(0);
    }

    debug_assert_eq!(v.len(), desired_len);
}

#[test]
fn test_pad_twice_v1() {
    let v = vec![1];
    let v = pad_with_zeros_v1(v, 2);
    let v = pad_with_zeros_v1(v, 4);
    assert_eq!(v, vec![1, 0, 0, 0]);
}

#[test]
fn test_pad_twice_v2() {
    let v = vec![1];
    let v = pad_with_zeros_v2(&v, 2);
    let v = pad_with_zeros_v2(&v, 4);
    assert_eq!(v, vec![1, 0, 0, 0]);
}

#[test]
fn test_pad_twice_v3() {
    let mut v = vec![1];
    pad_with_zeros_v3(&mut v, 2);
    pad_with_zeros_v3(&mut v, 4);
    assert_eq!(v, vec![1, 0, 0, 0]);
}


/*
    Problem 7: Move semantics continued

    Write a function which appends a row to a vector of vectors.
    Notice that it takes ownership over the row.
    You shouldn't need to use .clone().

    Why is this more general than being passed a &[bool]
    and cloning it?

    Second, write a function which returns whether
    a row equals the first row in the vector of vectors.
    Notice that it does not take ownership over the row.

    Why is this more general than being passed a Vec<bool>?
*/

pub fn append_row(grid: &mut Vec<Vec<bool>>, row: Vec<bool>) {
    grid.push(row);
}

pub fn is_first_row(grid: &[Vec<bool>], row: &[bool]) -> bool {
    assert!(!grid.is_empty(), "The grid is empty!");
    assert!((grid[0].len() == row.len()), "Dimensions mismatch");

    for (i, elt) in row.iter().enumerate().take(grid[0].len())
    {
        if *elt != row[i]
        {
            return false;
        }
    }

    true
    // Check if row is the first row in grid
    // Remember to handle the case when grid is empty
}


#[test]
fn test_append_row() {
    let mut v = vec![vec![true, false, true], vec![false, true, true]];
    let to_append = vec![true, true, false];

    append_row(&mut v, to_append);

    assert_eq!(v, vec![vec![true, false, true], vec![false, true, true], vec![true, true, false]]);
}


#[test]
fn test_is_first_row() {
    let v = vec![vec![true, false, true], vec![false, true, true]];

    assert_eq!(is_first_row(&v, &vec![true, false, true]), true);
}


/*
    Problem 8: Modifying while iterating

    In C and C++, you run into subtle bugs if you try to modify a data
    structure while iterating over it. Rust's move semantics prevents that.
*/

use std::collections::HashMap;

// To familiarize yourself with HashMaps,
// implement the following function which converts pairs from a slice
// into key-value pairs in a hashmap.
// Documentation:
// https://doc.rust-lang.org/std/collections/struct.HashMap.html

pub fn vector_to_hashmap(v: &[(i32, String)]) -> HashMap<i32, String> {
    let mut return_map : HashMap<i32, String> = HashMap::new();

    for (key, value) in v
    {
        return_map.insert(*key, value.to_string());
    }

    return_map
}


#[test]
fn test_vector_to_hashmap()
{

    let to_hashmap : Vec<(i32, String)> = vec![(5, "Hello".to_string()), (10, "foo".to_string()), (1, "bar".to_string())];

    let mut expected_output : HashMap<i32, String> = HashMap::new();
    expected_output.insert(5, "Hello".to_string());
    expected_output.insert(10, "foo".to_string());
    expected_output.insert(1, "bar".to_string());

    assert_eq!(vector_to_hashmap(&to_hashmap), expected_output);
}


// Now rewrite this function to delete all entries in hashmap where the keys
// are negative.
pub fn delete_negative_keys(h: &mut HashMap<i32, i32>) {
    // This fails, uncomment to see error.
    // for k in h.keys() {
    //     if *k < 0 {
    //         h.remove(k);
    //     }
    // }
    let mut keys_to_remove : Vec<i32> = Vec::new();
    for k in h.keys() {
        if *k < 0 {
            keys_to_remove.push(*k);
        }
    }

    for k in keys_to_remove
    {
        h.remove(&k);
    }
}

#[test]
fn test_delete_negative_keys()
{
    let mut to_delete : HashMap<i32,i32> = HashMap::new();
    to_delete.insert(-1, 5);
    to_delete.insert(5, 5);
    to_delete.insert(-2, 5);
    to_delete.insert(-3, 5);
    to_delete.insert(9, 5);

    let mut expected_output : HashMap<i32, i32> = HashMap::new();
    expected_output.insert(5, 5);
    expected_output.insert(9, 5);

    delete_negative_keys(&mut to_delete);


    assert_eq!(to_delete, expected_output);
}

/*
    Problem 9: The Entry API

    Move semantics present interesting API design choices not found in other
    languages.
    HashMap is an example of such a API.
    Specifically, the Entry API:
    https://doc.rust-lang.org/std/collections/hash_map/enum.Entry.html

    This allows for efficient HashMap access because we only access
    the entry in the map (computing an expensive hash function) once.

    Implement a function which does the following:
        For all entries in `add`: (k, v)
        If `k` exists in `merged`, append `v` to the value of `merged[k]`.
        If that `k` doesn't exist in `merged`, add the (k, v) to `merged`.
    Use `or_insert` and `and_modify`.
*/

pub fn merge_maps(
    merged: &mut HashMap<String, String>,
    add: HashMap<String, String>,
) {
    for (k, v) in add
    {
        merged.entry(k).and_modify(|e| {*e = v.clone()}).or_insert(v);
    }
}

#[test]
fn test_merge_maps_empty()
{
    let mut merged : HashMap<String, String> = HashMap::new();
    let mut add : HashMap<String, String> = HashMap::new();

    add.insert("foo".to_string(), "test1".to_string());
    add.insert("bar".to_string(), "test2".to_string());

    merge_maps(&mut merged, add.clone());
    // Note that cloning is not memory efficient although if merge_maps accepted a reference to a HashMap we could just pass a reference instead of copying it

    assert_eq!(merged, add);
}

#[test]
fn test_merge_maps_replace()
{
    let mut merged : HashMap<String, String> = HashMap::new();
    let mut add : HashMap<String, String> = HashMap::new();
    let mut expected_output : HashMap<String, String> = HashMap::new();

    merged.insert("foo".to_string(), "test1".to_string());
    merged.insert("foobar".to_string(), "test4".to_string());

    add.insert("foo".to_string(), "test3".to_string());
    add.insert("bar".to_string(), "test2".to_string());

    expected_output.insert("foo".to_string(), "test3".to_string());
    expected_output.insert("foobar".to_string(), "test4".to_string());
    expected_output.insert("bar".to_string(), "test2".to_string());

    merge_maps(&mut merged, add.clone());
    // Note that cloning is not memory efficient although if merge_maps accepted a reference to a HashMap we could just pass a reference instead of copying it

    assert_eq!(merged, expected_output);
}
